#!/usr/bin/env python3

import argparse
from wand.display import display

from muki.img import MukiImage
from muki.muki_manager import MukiManager

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Uploads new content to your Muki')
    parser.add_argument('file', help='image file to upload')
    parser.add_argument('--dry-run', action='store_true', help='only show the picture, not send it to Muki')
    parser.set_defaults(dry_run=False)
    parser.add_argument('--crop', action='store_true', help='crop the image to fit better to Muki')
    parser.set_defaults(crop=False)
    args = parser.parse_args()

    mm = MukiManager()

    with MukiImage(filename=args.file) as md:
        if args.dry_run:
            # display(md)
            with md.as_in_muki(crop=args.crop) as muki_img:
                display(muki_img)
        else:  # Send image to every Muki:
            muki = mm.discover_muki(timeout=120)

            if muki:
                print("Flashing %s..." % muki.name)
                mm.send_image(muki, md, crop=args.crop)
