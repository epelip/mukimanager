# mukimanager
In 2016, Paulig (Finnish drink company) [launced a new IOT coffee mug called Muki](https://www.pauliggroup.com/news/limited-edition-of-the-new-smart-coffee-cup-paulig-muki-now-available).
Muki has an integrated e-ink display with a resolution of 176 x 264 pixels. It is powered by coffee and generates the
required energy from the heat. With that energy, appears as a Bluetooth Low Energy device and can receive image updates
via GATT protocol.

Unfortunately, the app is no longer available for download. The source code of the mobile app is still
[available](https://github.com/gustavpaulig/Paulig-Muki). However, the application is dependent of backend service,
which is not running anymore. The Mukis themselves are fully functional so the lack of library implementations is really
a shame. The aim of this project is to provide an easy-to-use library for Muki, to continue the story of Muki.

This project is heavily inspired by [mukinator](https://github.com/jku/mukinator), which introduces how to communicate
with a Muki device. Dbus is not so beginner-friendly with its API, so I decided to write a python library which tries
to abstract majority of the low level communication away and provide an easier platform to build new creative
applications for Mukis.
