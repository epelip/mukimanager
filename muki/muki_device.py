class MukiDevice:
    def __init__(self, name: str, address: str, bonded: bool, rssi: int, dbus_path: str):
        self._name = name
        self._address = address
        self._bonded = bonded
        self._rssi = rssi
        self._dbus_path = dbus_path

    @property
    def name(self) -> str:
        return self._name

    @property
    def address(self) -> str:
        return self._address

    """Previously connected"""

    @property
    def bonded(self) -> bool:
        return self._bonded

    @property
    def rssi(self) -> int:
        return self._rssi

    @property
    def dbus_path(self) -> str:
        return self._dbus_path
