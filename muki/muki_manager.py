
import time
import dbus
from dbus.exceptions import DBusException
from dbus.mainloop.glib import DBusGMainLoop
from gi.repository import GLib

from typing import Callable, Union

from muki.img import MukiImage
from muki.muki_device import MukiDevice


# Bluez GATT API documentation:
# https://github.com/bluez/bluez/blob/master/doc/gatt-api.txt


# Explicit constructors are needed because otherwise
# the signature might not be guessable
def _dbus_array(value):
    return dbus.Array(value, signature="y")


def _dbus_dict(value):
    return dbus.Dictionary(value, signature="sv")


class MukiManager:
    def __init__(self, debug: bool = False, adapter_path: str = '/org/bluez/hci0'):
        DBusGMainLoop(set_as_default=True)
        self._mainloop = GLib.MainLoop()

        self._debug = debug

        # Initialize bluez
        self.bus = dbus.SystemBus()
        self.adapter = dbus.Interface(self.bus.get_object('org.bluez', adapter_path), 'org.bluez.Adapter1')
        self.bluez_manager = dbus.Interface(
            self.bus.get_object('org.bluez', '/'),
            'org.freedesktop.DBus.ObjectManager',
        )

    def _debug_print(self, text: str) -> None:
        if self._debug:
            print(text)

    def _write_to_muki(self, image_bytes: bytearray, characteristic: dbus.Interface):
        self._debug_print("Writing image to Muki...")

        characteristic.WriteValue(
            _dbus_array([0x74]),
            _dbus_dict({}),
        )

        # Write image in 291 chunks, 20 bytes at time
        index = 0
        for i in range(0, 291):
            data = image_bytes[index:index + 20]
            # last one may be too short
            while len(data) < 20:
                data.append(0xFF)

            # Slow down a bit so Muki can keep up receiving :D
            time.sleep(0.01)

            characteristic.WriteValue(
                _dbus_array(data),
                _dbus_dict({}),
            )
            index = index + 20

        characteristic.WriteValue(
            _dbus_array([0x64]),
            _dbus_dict({}),
        )

        self._debug_print("Write finished!")

        self._mainloop.quit()

    def _handle_new_interfaces(
            self,
            image_bytes: bytearray,
    ) -> Callable[[str, dict[any, any]], None]:
        def _handle(path: str, interfaces: dict[any, any]):
            if 'org.bluez.GattCharacteristic1' not in interfaces.keys():
                return

            char_object = self.bus.get_object('org.bluez', path)
            props = char_object.GetAll(
                'org.bluez.GattCharacteristic1',
                dbus_interface='org.freedesktop.DBus.Properties',
            )
            uuid = props['UUID']
            if uuid.lower() != '06640002-9087-04a8-658f-ce44cb96b4a1':
                return

            flags = props['Flags']
            self._debug_print("Located MukiManager characteristic %s [%s]" % (uuid, ', '.join(flags)))

            self._write_to_muki(
                image_bytes=image_bytes,
                characteristic=dbus.Interface(char_object, 'org.bluez.GattCharacteristic1'),
            )

        return _handle

    def list_known_mukis(self) -> list[MukiDevice]:
        objects = self.bluez_manager.GetManagedObjects()

        found_mukis: list[MukiDevice] = []

        for path, interfaces in objects.items():
            if 'org.bluez.Device1' not in interfaces.keys():
                continue

            address = str(interfaces['org.bluez.Device1']['Address'])

            if 'Name' not in interfaces['org.bluez.Device1'].keys():
                pass

            name = str(interfaces['org.bluez.Device1']['Name'])

            if name.startswith('PAULIG_MUKI_'):
                self._debug_print("Found muki %s (%s)" % (name, address))
                found_mukis.append(
                    MukiDevice(
                        name=name,
                        address=address,
                        bonded=bool(interfaces['org.bluez.Device1']['Bonded']),
                        rssi=99,  # int(interfaces['org.bluez.Device1']['RSSI']),
                        dbus_path=path,
                    )
                )

        return found_mukis

    def discover_muki(self, timeout: int = 30) -> Union[MukiDevice, None]:
        def stop_scan():
            self.adapter.StopDiscovery()
            self._mainloop.quit()

        self.found_muki = None

        def on_muki_discovered(path: str, interfaces: dict[any, any]):
            if 'Name' not in interfaces['org.bluez.Device1']:
                return

            name = interfaces['org.bluez.Device1']['Name']
            address = interfaces['org.bluez.Device1']['Address']

            if name.startswith('PAULIG_MUKI_'):
                self._debug_print("Found muki %s (%s)" % (name, address))
                self.found_muki = MukiDevice(
                    name=name,
                    address=address,
                    bonded=bool(interfaces['org.bluez.Device1']['Bonded']),
                    rssi=99,  # int(interfaces['org.bluez.Device1']['RSSI']),
                    dbus_path=path,
                )

                stop_scan()

        signal = self.bluez_manager.connect_to_signal(
            'InterfacesAdded',
            on_muki_discovered,
        )

        GLib.timeout_add_seconds(timeout, stop_scan)
        self.adapter.StartDiscovery()

        self._mainloop.run()
        signal.remove()

        return self.found_muki

    def send_image(self, muki: MukiDevice, image: MukiImage, crop: bool = False) -> bool:
        img_bytes = image.as_muki_bytes(crop=crop)
        assert len(img_bytes) == 5808  # Just to be safe ;)

        device = None
        signal = None
        try:
            device = dbus.Interface(
                self.bus.get_object('org.bluez', muki.dbus_path),
                'org.bluez.Device1',
            )

            # Interface handler for selected image
            handle_interfaces = self._handle_new_interfaces(img_bytes)

            # handle any new Muki characteristics that appear
            signal = self.bluez_manager.connect_to_signal(
                'InterfacesAdded',
                handle_interfaces,
            )

            device.Connect(timeout=5)
            self._mainloop.run()

        except DBusException:
            self._debug_print("Failed to connect Muki")
            return False

        finally:
            if device is not None:
                device.Disconnect()
            if signal is not None:
                signal.remove()
