import math

from wand.image import Image
from wand.color import Color

MUKI_HEIGHT = 264
MUKI_WIDTH = 176


class _ImgAsInMuki(Image):
    def __init__(self, image, crop: bool = False):
        super().__init__(width=MUKI_WIDTH, height=MUKI_HEIGHT, background=Color('white'))
        self._base_image = image
        self._crop = crop

    def __enter__(self):
        with Image(self._base_image) as raw_img:
            if self._crop:
                if (raw_img.height / raw_img.width) < (MUKI_HEIGHT / MUKI_WIDTH):
                    raw_img.transform(resize="x%d" % (MUKI_HEIGHT,))
                else:
                    raw_img.transform(resize="%dx" % (MUKI_WIDTH,))
            else:
                raw_img.transform(resize="%dx%d" % (MUKI_WIDTH, MUKI_HEIGHT))
            self.composite(raw_img, gravity='center')

        self.crop(width=MUKI_WIDTH, height=MUKI_HEIGHT, gravity='center')
        self.type = 'bilevel'  # Only black or white pixels

        return self


class MukiImage(Image):
    def as_in_muki(self, crop: bool = False):
        return _ImgAsInMuki(self, crop=crop)

    def as_muki_bytes(self, crop: bool = False):
        with self.as_in_muki(crop=crop) as img:
            img.rotate(90)  # The screen in Muki is rotated :)
            return _image_to_one_bit_byte_array(img)  # A binary array of one-bit pixels


def _image_to_one_bit_byte_array(img: Image) -> bytearray:
    pixels = bytearray(math.ceil(img.width * img.height / 8))
    bit_index = 0
    byte_index = 0
    for row in img:
        for pixel in row:
            is_black = pixel.red_int8 == 0 and pixel.green_int8 == 0 and pixel.blue_int8 == 0
            pixels[byte_index] = pixels[byte_index] | (is_black << bit_index)

            bit_index = bit_index + 1

            if bit_index == 8:
                byte_index = byte_index + 1
                bit_index = 0

    return pixels
